#  Andrew Coates
# 
#  This module is developed to defeat all the other kids robots.
#  Mostly by going straight.

import RPi.GPIO as GPIO
import time

#Set mode to the board.
GPIO.setmode(GPIO.BOARD)


#Tell the raspberry pi that we're going to be setting these pins.

GPIO.setup(40,GPIO.OUT) # This controls the left  wheel.
GPIO.setup(38,GPIO.OUT) # This controls the right wheel.

#Spin the robot right for 5 seconds.
GPIO.output(38,True)
time.sleep(5)
GPIO.output(38,False)

#Spin the robot left for 5 seconds.
GPIO.output(40,True)
time.sleep(5)
GPIO.output(40,False)


#Go straight for 2 seconds.                                                                                                
GPIO.output(38,True)
GPIO.output(40,True)
time.sleep(2)
GPIO.output(38,False)
GPIO.output(40,False)

#Spin the robot right for 5 seconds.                                                                                                 
GPIO.output(38,True)
time.sleep(5)
GPIO.output(38,False)

#Spin the robot left for 5 seconds.                                                                                                  
GPIO.output(40,True)
time.sleep(5)
GPIO.output(40,False)
