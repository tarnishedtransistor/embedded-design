import sys,tty,termios

class _Getch:
    def __call__(self):
            fd = sys.stdin.fileno()
            old_settings = termios.tcgetattr(fd)
            try:
                tty.setraw(sys.stdin.fileno())
                ch = sys.stdin.read(3)
            finally:
                termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
            return ch

def get():
        inkey = _Getch()
        while(1):
                k=inkey()
                if k!='':break
        if k=='\x1b[A':
                print "up"
                GPIO.output(40,True)
                GPIO.output(38,True)
                time.sleep(.2)
                GPIO.output(40,False)
                GPIO.output(38,False)
        elif k=='\x1b[C':
                print "right"
                GPIO.output(38,True)
                time.sleep(.2)
                GPIO.output(38,False)
        elif k=='\x1b[D':
                print "left"
                GPIO.output(40,True)
                time.sleep(.2)
                GPIO.output(40,False)
        else:
                print "not an arrow key!"

def main():
        for i in range(0,100):
                get()

if __name__=='__main__':
        main()
